import random
import string

def generate_random_email():
    name = ''.join(random.choice(string.ascii_letters) for _ in range(8))
    domain = ''.join(random.choice(string.ascii_lowercase) for _ in range(5))
    local = ''.join(random.choice(string.ascii_lowercase) for _ in range(3))
    
    email = f"{name}@{domain}.{local}"
    return email

def save_to_file(email):
    with open("rand_email.txt", "w") as file:
        file.write(email)

if __name__ == "__main__":
    random_email = generate_random_email()
    print("Генерация email:", random_email)
    
    save_to_file(random_email)
    print("Email сохранен в rand_email.txt")
