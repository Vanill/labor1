FROM python:3.9
RUN pip install psycopg2
WORKDIR /app
COPY main.py /app
CMD [ "python", "main.py" ]

